
def escribir_lista (nombre_fichero,lista):
    with open(nombre_fichero,'w') as descriptor_fich: 
        lista=descriptor_fich.writelines(lista)
        
def imprimir_lista (lista):
    
    for c in lista:
        print(c)

if __name__=="__main__":
    lista_entrada=['A','B','C','D','E','F','G','H','I','J','K']
    lista_salida=[]
    
    for elemento in lista_entrada:
        lista_salida.append(elemento+ ':'+str(lista_entrada.index(elemento)+1)+'\n')
    imprimir_lista (lista_salida)
    escribir_lista('salida-ej3.txt',lista_salida)
